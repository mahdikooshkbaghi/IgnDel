# IgnDel Python Package
The ignition time is defined as the corresponding time for maximum of
dT/dt [K/sec]. The first order Spline is fitted to the T-time curve
while the differentiation is done locally (analytically) from the 
spline curve.
## How to use:
First convert the CHEMKIN mechanisms with the following built-in routine
of Cantera:

**ck2cti --input=chem.inp --thermo=therm.dat --transport=trans.dat**

The output is *'chem.cti'* which is the proper input for Cantera.

The name of the mechanism (Cantera built in or CHEMKIN translated one)
should be given as **reaction_mechanism = 'foo.cti'** in the source code.

The **input.var** includes all necessary parameters of interest in ignition
delay time calculation.

In the current version the hydrocarbon oxidation is considered as: 

**CxHy + (x+y/4)/phi (O2+79/21 N2)**

The proper coefficients and name of the species should be given in
source code in front of **gas.TPX** deceleration.
## How to cite:
Please cite the following paper:

Kooshkbaghi, M., Frouzakis, C. E., Boulouchos, K., & Karlin, I. V. (2014). 
*Entropy production analysis for mechanism reduction.* 
Combustion and Flame, 161(6), 1507-1515. 
[DOI](http://dx.doi.org/10.1016/j.combustflame.2013.12.016)
