__author__ = 'Mahdi Kooshkbaghi'
import csv
import cantera as ct
import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.interpolate import UnivariateSpline

# Reading the input file input.var
input_var = []
with open("input.var", "r") as txt:
    for line in txt:
        if not (line[0]=='!'):
           input_var.append(line)

with open('samples.txt', 'w') as output:
    output.close()

reaction_mechanism = 'gri30.cti'
phi     = float(input_var[0])       # [-]
p       = float(input_var[1])       # [atm]
T_in_min= float(input_var[2])       # [K]
T_in_max= float(input_var[3])       # [K]
step_T  = float(input_var[4])       # [K]
Trange  = np.linspace(T_in_min, T_in_max, num =step_T)
t_final = float(input_var[5])       # [s]
dt      = float(input_var[6])
num     = 10000   # number of interpolation points
t_ign   = []      # empty list for ignition delay times
# Loading a mechanism
gas = ct.Solution(reaction_mechanism)
# Find the number of species and reactions
ns = gas.n_species
nr = gas.n_reactions
print ("The mechanism includes {} number of species and {} number of \
reactions".format(ns, nr))

print ('{:10}\t{:10}'.format('T [K]','ign [s]'))

for T in Trange:
    # Setting the state
    # Temperature and pressure
    gas.TPX = T, p*ct.one_atm, \
            {'CH4':1, 'O2': 2.0/phi, 'N2':2.0*(79./21.)/phi}
    r = ct.IdealGasConstPressureReactor(gas)
    sim = ct.ReactorNet([r])
    time = 0.0
    tt = []     # time
    TT = []     # temperature
    YY = []     # mass fractions
    while time<t_final:
        time = time + dt
        sim.advance(time)
        tt.append(time)
        TT.append(r.T)
        YY.append(r.Y)

    # Generate the sample datasets for further analysis
    out_var=zip(TT, YY)
    with open('samples.txt', 'a') as output:
        for elem in out_var:
            output.write('{} {}\n'.format(elem[0], \
                    ' '.join(str(i) for i in elem[1])))

    # Fit a curve on T-t ignition history
    f      = UnivariateSpline(tt, TT)
    # Find dT/dt
    df     = f.derivative()
    tt_new = np.linspace(tt[0], tt[-1], num=num, endpoint=True)

    i = 0
    ign_time = tt_new[i]
    while i<=num-1:
        if df(tt_new[i])>df(ign_time):
            ign_time = tt_new[i]
        i = i+1
    t_ign.append(ign_time)
    print ('{:8.4f}\t{:8f}'.format(T,ign_time))

# Print the ignition delay time vs T0
Trange = np.array(Trange)
t_ign  = np.array(t_ign)
np.savetxt(str(reaction_mechanism)+'_Species'+str(ns)+'_phi'+ \
        str(phi)+'_Press'+str(p)+'_ign_del.txt', \
        np.transpose([Trange,t_ign]))

# Mathplotlib
# Plot the ignition delay vs 1000/T for T in Trange
plt.plot(1000./Trange,t_ign,'k-o')
plt.yscale('log')
plt.xlabel('1000/T [1/K]', fontsize = 20)
plt.ylabel('Ignition delay time [s]', fontsize = 20)
plt.savefig('tau_T.pdf', format='pdf', dpi=1000,bbox_inches='tight')
plt.show()
